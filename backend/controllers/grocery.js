import User from '../models/user';
import jwt from 'jsonwebtoken'
import byscript from "bcryptjs"
import nodemailer from "nodemailer"

export const getAllGroceries = async(req, res, next) => {
    try {
        const [allGroceries] = await User.fetchAll();
        res.status(200).json(allGroceries);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

export const postGrocery = async(req, res, next) => {
    jwt.verify(req.token, 'secretkey', async(error, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            try {
                // const postResponse = await User.post(req.body.item);
                res.status(201).json({ data: [], auth: authData });
            } catch (err) {
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            }
        }
    })
};

export const putGrocery = async(req, res, next) => {
    try {
        const putResponse = await User.update(req.body.id, req.body.item);
        res.status(200).json(putResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

export const deleteGrocery = async(req, res, next) => {
    try {
        const deleteResponse = await User.delete(req.params.id);
        res.status(200).json(deleteResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

export const verifyToken = (req, res, next) => {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }

}

export const login = async(req, res, next) => {
    try {
        const email = req.body.email;
        const password = req.body.password;


        const user = await User.find(email)

        const isTheSame = await byscript.compare(password, user[0][0].password)

        if (user == null || !isTheSame) {
            res.sendStatus(500);
        } else {

            jwt.sign({ user }, 'secretkey', (req, token) => {
                res.json({
                    token: token
                })
            });

        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

export const registrar = async(req, res, next) => {
    try {
        const name = req.body.name
        const email = req.body.email
        const password = req.body.password
        let passHash = await byscript.hash(password, 8)
        const departament = req.body.departament

        await User.post(name, email, passHash, departament);
        res.status(201).json({ data: "creado" });

    } catch (err) {
        console.log("error", err)
        res.sendStatus(500);
        next(err);
    }
};

export const enviarMensaje = async(req, res, next) => {
    try {
        let invitado = await User.findInvitado(req.body.email)
        let invitadoId;
        if (!invitado[0].length) {
            invitado = await User.crearInvitado(req.body.name, req.body.email)
            invitadoId = invitado[0].insertId
            console.log("Creo el invitado", invitadoId)
        } else {
            console.log("Entro en el else")
            invitadoId = invitado[0][0].id
        }

        console.log("Llego aqui ", invitadoId)
        const respuesta = await User.crearMensaje(3, invitadoId, new Date(), new Date(), req.body.mensaje, false);
        res.status(200).json(respuesta);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

export const obtenerTodosMensajes = async(req, res, next) => {
    try {
        const mensajes = await User.obtenerMensaje(3)
        res.status(200).json(mensajes);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

export const actualizarStatus = async(req, res, next) => {
    try {
        console.log("llego aqui")
        const mensajes = await User.updateStatusDeMensaje(req.body.id)
        res.status(200).json(mensajes);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};


export const enviarEmail = async(req, res, next) => {
    try {
        var transporter = nodemailer.createTransport({

            host: "smtp-relay.sendinblue.com",
            port: 587,
            secure: false,
            auth: {
                user: "wil-19-60@live.com",
                pass: `a@4DF8a"$+z-q=W3`
            },
        })

        var mailOption = {
            from: "Remitente@gmail",
            to: "r@gmail.com",
            subject: "prueba",
            text: "hola"
        }

        transporter.sendMail(mailOption, (error, info) => {
            if (error) {
                console.log("error", error)
                throw new Error()
            } else {
                console.log("Correo enviado")
                res.status(200).json({ data: "bien" });
            }
        })

        res.status(200).json(mensajes);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};