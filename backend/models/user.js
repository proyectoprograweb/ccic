import db from '../util/database';

export default class User {
    constructor(id, item) {
        this.id = id;
        this.item = item;
    }

    static find(email) {
        return db.query(`SELECT * FROM mydb.Users WHERE email = '${email}'`);
    }

    static findInvitado(email) {
        return db.query(`SELECT * FROM mydb.Invitado WHERE email = '${email}'`);
    }

    static post(name, email, password, departament) {
        return db.query("INSERT INTO Users SET ?", { name: name, email: email, password: password, fk_departmentId: departament }, (err, suc) => {
            console.log("err: ", err)
        });
    }

    static update(id, item) {
        return db.execute('UPDATE groceries SET item = ? WHERE id = ?', [item, id]);
    }

    static delete(id) {
        return db.execute('DELETE FROM groceries WHERE id = ?', [id]);
    }

    static crearInvitado(name, email) {
        return db.query("INSERT INTO mydb.Invitado SET ?", { name: name, email: email }, (err, suc) => {
            console.log("sucess: ", suc)
            console.log("err: ", err)
        });
    }

    static crearMensaje(fk_departmentId, fk_invitadoId, respuestaDia, revisadoDia, mensaje, estaRevisado) {
        return db.query("INSERT INTO mydb.Mensaje SET ?", { mensaje: mensaje, estaRevisado: estaRevisado, revisadoDia: revisadoDia, respuestaDia: respuestaDia, fk_invitadoId: fk_invitadoId, fk_departmentId: fk_departmentId }, (err, suc) => {
            console.log("suc: ", suc)
            console.log("err: ", err)
        });
    }

    static obtenerMensaje(departamentId) {
        return db.query(`SELECT M.*, I.name, I.email, 'TLC' as fk_departmentId  FROM Mensaje M inner JOIN Invitado I on I.id = M.fk_invitadoId`);
    }

    static updateStatusDeMensaje(id) {
        return db.query(`UPDATE mydb.Mensaje SET estaRevisado = 1 WHERE id = ${id}`);
    }
};