import { Router } from 'express';

import { getAllGroceries, postGrocery, putGrocery, deleteGrocery, verifyToken, login, registrar, enviarMensaje, obtenerTodosMensajes, actualizarStatus, enviarEmail } from '../controllers/grocery';

const router = Router();

router.get('/', verifyToken, getAllGroceries);

router.post('/', verifyToken, postGrocery);

router.put('/', verifyToken, putGrocery);

router.delete('/:id', verifyToken, deleteGrocery);

router.post('/login', login);

router.post('/registrar', registrar);

router.post('/registrarPregunta', enviarMensaje);

router.get('/mostrarPreguntas', obtenerTodosMensajes);

router.put("/actualizarStatus", actualizarStatus)




export default router;