CREATE DATABASE `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE `Department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `Invitado` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `Users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` longtext,
  `fk_departmentId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Department_idx` (`fk_departmentId`),
  CONSTRAINT `fk_Department` FOREIGN KEY (`fk_departmentId`) REFERENCES `Department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



CREATE TABLE `Mensaje` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mensaje` longtext NOT NULL,
  `multimedia` varchar(45) DEFAULT NULL,
  `estaRevisado` tinyint NOT NULL,
  `revisadoDia` datetime NOT NULL,
  `respuestaDia` datetime DEFAULT NULL,
  `fk_invitadoId` int DEFAULT NULL,
  `fk_departmentId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invitadoId_idx` (`fk_invitadoId`),
  KEY `fk_departmentId_idx` (`fk_departmentId`),
  CONSTRAINT `fk_departmentId` FOREIGN KEY (`fk_departmentId`) REFERENCES `Department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_invitadoId` FOREIGN KEY (`fk_invitadoId`) REFERENCES `Invitado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3;

